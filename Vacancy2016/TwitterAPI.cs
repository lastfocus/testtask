﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using TweetSharp;

namespace Vacancy2016
{
    public static class TwitterAPI
    {
        public static string TestTwitter(string url)
        {
            var consumerKey = ConfigurationManager.AppSettings["consumerKey"];
            var consumerSecret = ConfigurationManager.AppSettings["consumerSecret"];
            var accessToken = ConfigurationManager.AppSettings["accessToken"];
            var accessTokenSecret = ConfigurationManager.AppSettings["accessTokenSecret"];

            var service = new TwitterService(consumerKey, consumerSecret);
            service.AuthenticateWith(accessToken, accessTokenSecret);

            var options = new SendTweetOptions() { Status = url };
            TwitterStatus result = service.SendTweet(options);
            return (result != null) ? result.Text : "error";
        }
    }
}
