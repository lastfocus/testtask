﻿
using System.Windows;
using System.Globalization;
using System.Resources;
using System.Threading;

namespace Vacancy2016
{
    public partial class MainWindow : Window
    {

        public CultureInfo cul = CultureInfo.CreateSpecificCulture("ru");
        public ResourceManager resMan;        

        public MainWindow()
        {
            resMan = new ResourceManager("Vacancy2016.Names", typeof(MainWindow).Assembly);

            InitializeComponent();

            Renaming(cul);

            DataContext = new FileServiceViewModel();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            cul = CultureInfo.CreateSpecificCulture("ru");            
            Renaming(cul);            
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            cul = CultureInfo.CreateSpecificCulture("en");
            Renaming(cul);            
        }

        private void Renaming(CultureInfo cul)
        {
            Thread.CurrentThread.CurrentCulture = cul;
            Thread.CurrentThread.CurrentUICulture = cul;
            uploadButton.Content = resMan.GetString("button_text", cul);
            configurationMenu.Header = resMan.GetString("configuration_menu", cul);
            languageMenu.Header = resMan.GetString("language_menuitem", cul);
            //statusLabel.Content = "";
            
        }
    }
}
