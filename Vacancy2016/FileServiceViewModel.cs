﻿using System.ComponentModel;
using System.Windows;
using System.Globalization;
using System.Resources;
using System.Threading;


namespace Vacancy2016
{
    public class FileServiceViewModel : INotifyPropertyChanged
    {
        private string _status;
        private Command _uploadCommand;
        private Command _copyAddressCommand;
        private ResourceManager _rm;
        private string _publicUrl;                          

        public event PropertyChangedEventHandler PropertyChanged;

        public Command UploadCommand => _uploadCommand ?? (_uploadCommand = new Command(Upload));
        public Command CopyAddressCommand => _copyAddressCommand ?? (_copyAddressCommand = new Command(CopyAddress, false));
        public ResourceManager rm => _rm?? (_rm = new ResourceManager("Vacancy2016.Names", typeof(FileServiceViewModel).Assembly));
        

        public string status
        {
            get { return _status; }
            private set
            {
                if (_status != value)
                {
                    _status = value;
                    OnPropertyChanged(nameof(status));
                }
            }
        }


        private async void Upload()
        {
            _publicUrl = null;

            UploadCommand.CanExecute = false;
            CopyAddressCommand.CanExecute = false;
            
            status = rm.GetString("upload_status", Thread.CurrentThread.CurrentCulture);

            try
            {
                _publicUrl = await FileService.UploadRandomPattern();
            }
            catch
            {                
                status = rm.GetString("error_status", Thread.CurrentThread.CurrentCulture);
            }
                        
            if (_publicUrl != null)
            {
                string result = (TwitterAPI.TestTwitter(_publicUrl) != "error") ? rm.GetString("tweet_success", Thread.CurrentThread.CurrentCulture) : rm.GetString("tweet_error", Thread.CurrentThread.CurrentCulture);

                status = rm.GetString("success_status", Thread.CurrentThread.CurrentCulture) + " " + _publicUrl + "\n\r" + result;
                
                CopyAddressCommand.CanExecute = true;
                
            }

            UploadCommand.CanExecute = true;
        }

        private void CopyAddress()
        {
            if (_publicUrl != null)
            {
                Clipboard.SetText(_publicUrl);
            }
        }


        protected void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }       
        
    }
}
