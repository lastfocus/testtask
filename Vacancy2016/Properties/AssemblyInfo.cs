﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;

[assembly: AssemblyTitle("Vacancy2016")]
[assembly: AssemblyProduct("Vacancy2016")]
[assembly: AssemblyDescription("Тестовое задание")]
[assembly: AssemblyCompany("ООО \"Инженерный центр \"Энергосервис\"")]
[assembly: AssemblyCopyright("© ООО \"Инженерный центр \"Энергосервис\", 2015-2016")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyTrademark("")]

[assembly: ComVisible(false)]

[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]

[assembly: AssemblyVersion("1.0.0.0")]
