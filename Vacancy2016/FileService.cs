﻿using Flurl.Http;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Vacancy2016
{
    public static class FileService
    {
        private static readonly string OAuth = "OAuth AQAAAAAYPZ9uAANc4vfkDMVif0PbmrirEMRL_eQ";

        public static async Task<string> UploadRandomPattern()
        {
            var rnd = GetRandomBytes(16);
            var bytes = GetPatternBytes(rnd);
            var path = Uri.EscapeDataString($"app:/{ GetName(rnd) }.png");

            await Upload(bytes, await InitializeUpload(path));
            await Publish(path);

            return await GetPublicUrl(path);
        }


        private static async Task<string> InitializeUpload(string path)
        {
            var upload = await $"https://cloud-api.yandex.net/v1/disk/resources/upload?path={ path }&overwrite=true"
                .WithHeader("Authorization", OAuth)
                .GetJsonAsync();

            return (string)upload.href;
        }

        private static async Task<HttpStatusCode> Upload(byte[] bytes, string uploadHref)
        {
            var fileContent = new ByteArrayContent(bytes);
            fileContent.Headers.Add("Content-Type", "application/octet-stream");
            fileContent.Headers.Add("Content-Length", bytes.Length.ToString());

            var response = await uploadHref.PutAsync(fileContent);

            if (response.StatusCode != HttpStatusCode.Created)
            {
                throw new InvalidOperationException("Ошибка загрузки файла");
            }

            return response.StatusCode;
        }

        private static async Task<HttpResponseMessage> Publish(string path)
        {
            return await $"https://cloud-api.yandex.net/v1/disk/resources/publish?path={ path }"
                .WithHeader("Authorization", OAuth)
                .PutAsync(null);
        }

        private static async Task<string> GetPublicUrl(string path)
        {
            var meta = await $"https://cloud-api.yandex.net/v1/disk/resources?path={ path }"
                .WithHeader("Authorization", OAuth)
                .GetJsonAsync();

            return (string)meta.public_url;
        }


        private static byte[] GetRandomBytes(int count)
        {
            var rnd = new byte[count];

            using (var rg = RandomNumberGenerator.Create())
            {
                rg.GetBytes(rnd);
            }

            return rnd;
        }

        private static string GetName(byte[] rnd)
        {
            return BitConverter.ToString(rnd).Replace("-", "").ToLowerInvariant();
        }

        private static byte[] GetPatternBytes(byte[] rnd)
        {
            byte[] imageBytes;
            var bitmap = GetPattern(rnd);

            using (var stream = new MemoryStream())
            {
                bitmap.Save(stream, ImageFormat.Png);
                imageBytes = stream.ToArray();
            }

            return imageBytes;
        }

        private static Bitmap GetPattern(byte[] rnd)
        {
            int tileSize = 80;
            var tilesH = 8;
            var tilesV = 6;
            var bitmap = new Bitmap(tileSize * tilesH, tileSize * tilesV);

            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.FillRectangle(Brushes.White, 0, 0, bitmap.Width, bitmap.Height);

                for (int i = 0, j = 20; i < 10; i++, j += 4)
                {
                    using (var brush = new SolidBrush(Color.FromArgb(Math.Abs(rnd[i] - 127), rnd[0], rnd[1], rnd[i + 2])))
                    {
                        graphics.FillRectangle(brush, j, 0, 4, tileSize);
                    }
                }

                for (int i = 0, j = 0; i < 16; i++, j += 5)
                {
                    using (var brush = new SolidBrush(Color.FromArgb(Math.Abs(rnd[i] - 127), rnd[i], rnd[4], rnd[5])))
                    {
                        graphics.FillRectangle(brush, 0, j, tileSize, 5);
                    }
                }

                for (int i = 0; i < tilesH * tilesV; i++)
                {
                    graphics.DrawImageUnscaledAndClipped(bitmap, new Rectangle(i % tilesH * tileSize, i / tilesH * tileSize, tileSize, tileSize));
                }
            }

            return bitmap;
        }
    }
}
